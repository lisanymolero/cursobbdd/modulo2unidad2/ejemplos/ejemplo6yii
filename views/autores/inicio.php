<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\db\ActiveQuery;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Autores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autores-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
         'layout' => "{summary}\n{items}\n{pager}",
        'columns' => [
            'id',
            'nombre',
            'foto',
            [
            'label'=>'foto',
            'format'=>'raw',
            'value' => function($data){
                $url = Yii::getAlias("@web") . "/imgs/" . $data->foto;
                return Html::img($url,[
                    'width'=>'250',
                    'alt'=>'yii']); 
            }
            ],
                    // comente ésto porq no quiero que editen en esta vista
            // añadir boton mas...       
           /* [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'buttons' => [
                'view' => function ($url,$model) {
                    return Html::a(
                        'Mas...', 
                        ['autores/ver','id'=>$model->id],
                        ['class'=>"btn btn-primary"]
                    );
                },
	        ],
            ],*/
        ],
    ]); ?>
 <div class="col-lg-12s">
                <h2>Obras</h2>


                <p><?= Html::a("Ir a las Obras", ["autores/autoresobras",'id' => $dataProvider->models[0]->id], ["class" => "btn btn-default"]) ?></p>
            </div>

</div>

