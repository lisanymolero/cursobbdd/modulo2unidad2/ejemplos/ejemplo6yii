<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Autores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="autores-form">
     <?php $form = ActiveForm::begin(); ?>

  <?= $form->field($model, 'nombre')->textarea(['rows' => 3]) ?>

   <?= $form->field($model, 'foto')->fileInput() ?>
  
   
    <?php
    if(!is_null($model->foto)){
        echo Html::img('@web/imgs/' . $model->foto, [
        'alt' => 'My logo',
        'width'=>800,
        'class'=> 'img-responsive img-thumbnail'
        ]);
    }
    ?>
  
  <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>

   