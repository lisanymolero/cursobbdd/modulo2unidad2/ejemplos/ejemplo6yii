<?php
    use yii\helpers\Html;
    
?>
<div class="jumbotron">
<h1><?= $model->titulo ?></h1>
</div>
<div class="row">
<?= 
    Html::img('@web/imgs/' . $model->foto, [
    'alt' => 'My logo',
    'width'=>800,
    'class'=> 'img-responsive img-thumbnail'
    ]) 
       

?>
</div>
<div class="row">
<ul>
    <li>Descripcion: <?= $model->nombre?></li>
    
</ul>
</div>

