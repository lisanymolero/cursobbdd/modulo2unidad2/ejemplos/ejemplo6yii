<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Autores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="autores-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Autores', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
 

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
         'layout' => "{summary}\n{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            [
            'label'=>'Foto',
            'format'=>'raw',
            'value' => function($data){
                $url = Yii::getAlias("@web") . "/imgs/" . $data->foto;
                return Html::img($url,[
                    'width'=>'150',
                    'alt'=>'yii']); 
            }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
