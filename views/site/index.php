
<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Gestion de Libros y Autores';
?>


<div class="site-index">

    <div class="jumbotron">
        <h1> Gestión de libros y autores</h1>
    
    </div>
    
    <div class="col-lg-12 ">
               
               <?= Html::img('@web/imgs/Libreria-Obidos-Iglesia-4.jpg', ['alt' => 'My logo', "class" => "centrado"]) ?>
</div>

    <div class="body-content">

        <div class="row">
            
            <div class="col-lg-3 ">
                
            </div>
       
            <div class="col-lg-3 ">
                <h2>Autores</h2>


                <p><?= Html::a("Ir a la tabla Autores", ["autores/inicio"], ["class" => "btn btn-default" ,"centrado"]) ?></p>
            </div>
            
            <div class="col-lg-3">
                <h2>Libros</h2>


                <p><?= Html::a("Ir a la Lista de libros", ["libros/inicio"], ["class" => "btn btn-default"]) ?></p>
            </div>
            
              <div class="col-lg-3 ">
                
            </div>
            
            
            
        </div>

    </div>
</div>
