<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\db\ActiveQuery;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Autores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="libros-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= GridView::widget([
        'dataProvider' => $datos,
         'layout' => "{summary}\n{items}\n{pager}",
        'columns' => [
            
            [
            'label'=>'Imagen',
            'format'=>'raw',
            'value' => function($data){
                $url = Yii::getAlias("@web") . "/imgs/" . $data->portada;
                return Html::img($url,[
                    'width'=>'150',
                    'alt'=>'yii']); 
            }
            ],
            'nombre',
            'editorial',
             'autor',
                    
            ],
                    // comente ésto porq no quiero que editen en esta vista
            // añadir boton mas...       
           /* [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'buttons' => [
                'view' => function ($url,$model) {
                    return Html::a(
                        'Mas...', 
                        ['autores/ver','id'=>$model->id],
                        ['class'=>"btn btn-primary"]
                    );
                },
	        ],
            ],*/
        ]); ?>


</div>


