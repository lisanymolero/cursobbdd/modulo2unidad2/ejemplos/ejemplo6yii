<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\db\ActiveQuery;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista de Libros disponibles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="libros-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
         'layout' => "{summary}\n{items}\n{pager}",
        'columns' => [
          
             [
            'label'=>'portada',
            'format'=>'raw',
            'value' => function($data){
                $url = Yii::getAlias("@web") . "/imgs/" . $data->portada;
                return Html::img($url,[
                    'width'=>'250',
                    'alt'=>'yii']); 
            }
            ],
            
            'nombre',
               'autor0.nombre',
           
           
                    [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'buttons' => [
                'view' => function ($url,$model) {
                    return Html::a(
                        'Mas...', 
                        ['libros/obrasautor','id'=>$model->id],
                        ['class'=>"btn btn-primary"]
                    );
                },
	        ],
            ],
        ],
    ]); ?>

</div>


