<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Libros */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="libros-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'editorial')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'portada')->fileInput() ?>
    
     <?= $form->field($model, 'autor')->dropDownList(
            $autores,['prompt'=>'selecciona un autor...']); ?>
    
      <?php
    if(!is_null($model->portada)){
        echo Html::img('@web/imgs/' . $model->portada, [
        'alt' => 'My logo',
        'width'=>800,
        'class'=> 'img-responsive img-thumbnail'
        ]);
    }
    ?>

   
    
   

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>



 
  
   