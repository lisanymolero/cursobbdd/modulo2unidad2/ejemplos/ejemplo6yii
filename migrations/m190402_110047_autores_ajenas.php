<?php

use yii\db\Migration;

/**
 * Class m190402_110047_autores_ajenas
 */
class m190402_110047_autores_ajenas extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        
        
        // creates index for column `autores_id`
        $this->createIndex(
            'idx-post-autores_id',
            'libros',
            'autor'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fklibrosautor',
            'libros',
            'autor',
            'autores',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey("fklibrosautor", "libros");

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190402_110047_autores_ajenas cannot be reverted.\n";

        return false;
    }
    */
}
