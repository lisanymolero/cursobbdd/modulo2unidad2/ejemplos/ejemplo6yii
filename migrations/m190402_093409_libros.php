<?php

use yii\db\Migration;

/**
 * Class m190402_093409_libros
 */
class m190402_093409_libros extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
$this->createTable("libros", [
    'id'=>$this->primaryKey(),
    'nombre'=>$this->string(100),
    'editorial'=>$this->string(50),
    'portada'=>$this->string(25),
    'autor'=>$this->integer(),
        ]);
        
        $this->insert("libros", [
            'nombre'=>'Don Quijote de la Mancha',
            'editorial'=>'Libs',
            'autor'=>1,
            'portada'=>'libro1.jpg',
            
            ]);
        
         $this->insert("libros", [
            'nombre'=>'En busca del tiempo perdido',
            'editorial'=>'Alianza',
            'autor'=>2,
            'portada'=>'libro2.jpg',
            
            ]);
         
          $this->insert("libros", [
            'nombre'=>'Libro de los Gorriones',
            'editorial'=>'Facedicion',
            'autor'=>3,
            'portada'=>'libro3.jpg',
            
            ]);
        
       


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("libros");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190402_093409_libros cannot be reverted.\n";

        return false;
    }
    */
}
