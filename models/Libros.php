<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "libros".
 *
 * @property int $id
 * @property string $nombre
 * @property string $editorial
 * @property string $portada
 * @property int $autor
 *
 * @property Autores $autor0
 */
class Libros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'libros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['autor'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['editorial'], 'string', 'max' => 50],
            [['portada'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            [['autor'],'exist', 'skipOnError' => true, 'targetClass' => Autores::className(), 'targetAttribute' => ['autor' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Titulo',
            'editorial' => 'Editorial',
            'portada' => 'Portada',
            'autor' => 'Autor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutor0()
    {
        return $this->hasOne(Autores::className(), ['id' => 'autor']);
    }
    
     public function upload()
    {
             $this->portada->saveAs('imgs/' . $this->portada->baseName . '.' . $this->portada->extension);
        } 
        
}
